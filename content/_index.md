---
title: NAP
url: /
---
Velkommen til «NAP» - det norske tilgangspunktet for *åpne data* knyttet til tjenester- og data om transport, trafikk og mobilitet.
Du vil finne data fra både offentlige og private virksomheter i portalen. Les mer om «NAP» ved å følge linkene du finner på denne siden.
