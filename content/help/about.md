---
title: "Om NAP-portalen"
date: 2019-08-15T10:21:00+02:00
draft: false
url: /about/
---

Denne portalen inneholder pekere til datasett og tjenester som dreier seg om transport, trafikk og vegnettet i Norge. Eksempler kan være informasjon om vegnett, vegobjekter, stengninger, hendelser, trafikkmengde, reisetider, kollektivruter, reiseplaner, vær og føreforhold på vegene.

Portalen er et *nasjonalt tilgangspunkt* for trafikk og transportdata som følge av EUs direktiv 2010/40/EU – ITS-direktivet med underliggende forordninger. Disse er inntatt i den norske ITS-loven. [Alle land innenfor EU/EØS skal opprette et slikt tilgangspunkt](https://ec.europa.eu/transport/themes/its/road/action_plan/nap_en) for å støtte utvikling av sammenhengende informasjonstjenester i Europa.

Både offentlige og private virksomheter er forventet å bidra med datasett til portalen, og dette vil kunne være virksomheter som er vegeiere, opererer transport- og mobilitetstjenester såvel som formidlere av trafikkinformasjon.

Statens vegvesen er tildelt ansvaret for å iverksette og forvalte det nasjonale tilgangspunktet, og har mandat til å vurdere om et datasett faller inn under det som er spesifisert i forordningene knyttet til ITS-direktivet.

Katalogen over datasett er en integrert del av Brønnøysundregistrenes "Felles datakatalog for Norge".

## Til deg som vil få tilgang til data
Portalen inneholder kun *metadata* om datasettet dvs. data som beskriver datasettet og hvordan få tilgang. Datakildene er oftest plassert hos hver enkelt dataeier, og metadataene inneholder linker til disse.

Datasett knyttet til portalen skal fortrinnsvis være åpne. Dersom det foreligger betalingsbetingelser eller andre bruksvilkår, håndteres avtaler og fakturering av dataeier.

Alle datasett som knyttes opp mot portalen skal være i samsvar med General Data Protection Regulations (GDPR). Ingen personopplysninger skal kunne trekkes ut fra datasettene. Brudd på personvernlovgiving kan medføre reaksjoner.

Feil i data som oppdages av eier eller meldes av bruker skal rettes uten forsinkelse.
